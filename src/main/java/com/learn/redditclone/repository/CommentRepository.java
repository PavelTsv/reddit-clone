package com.learn.redditclone.repository;

import com.learn.redditclone.model.Comment;
import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findByPost(Post post);

    List<Comment> findByUser(User user);
}
