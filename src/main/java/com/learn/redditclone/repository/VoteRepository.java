package com.learn.redditclone.repository;

import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.User;
import com.learn.redditclone.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
    Optional<Vote> findByPostAndUser(Post postToUpvote, User currentUser);
}
