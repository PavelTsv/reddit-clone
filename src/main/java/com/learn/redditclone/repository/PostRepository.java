package com.learn.redditclone.repository;

import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.Subreddit;
import com.learn.redditclone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {
    List<Post> findAllBySubredditOrderByCreatedDate(Subreddit subreddit);

    List<Post> findByUser(User user);
}
