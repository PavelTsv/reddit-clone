package com.learn.redditclone.validators;

import com.learn.redditclone.dto.RegisterRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }

    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context) {
        RegisterRequest newUserRequest = (RegisterRequest) obj;
        return newUserRequest.getPassword().equals(newUserRequest.getConfirmationPassword());
    }
}
