package com.learn.redditclone.service;

import com.learn.redditclone.dto.PostRequest;
import com.learn.redditclone.dto.PostResponse;
import com.learn.redditclone.exceptions.PostNotFoundException;
import com.learn.redditclone.exceptions.SubredditNotFoundException;
import com.learn.redditclone.exceptions.UserNotFoundException;
import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.Subreddit;
import com.learn.redditclone.model.User;
import com.learn.redditclone.repository.PostRepository;
import com.learn.redditclone.repository.SubredditRepository;
import com.learn.redditclone.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PostService {
    private final SubredditRepository subredditRepository;
    private final AuthService authService;
    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Autowired
    public PostService(SubredditRepository subredditRepository,
                       AuthService authService,
                       PostRepository postRepository,
                       UserRepository userRepository) {
        this.subredditRepository = subredditRepository;
        this.authService = authService;
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    public Long save(PostRequest postRequest) {
        String subredditName = postRequest.getSubredditName();
        Subreddit subreddit = subredditRepository.findByName(subredditName)
                .orElseThrow(() -> new SubredditNotFoundException("No subreddit with name: " + subredditName + " found"));
        User user = authService.getCurrentUser()
                .orElseThrow(() -> new UserNotFoundException("User is not logged in"));
        Post post = postRepository.save(new Post(postRequest, subreddit, user));
        return post.getId();

    }

    @Transactional(readOnly = true)
    public PostResponse get(Long id) {
        Post post = postRepository.findById(id)
                .orElseThrow(() -> new PostNotFoundException("No post with id: " + id + " found"));
        return new PostResponse(post);
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getAll() {
        return postRepository.findAll().stream()
                .map(PostResponse::new)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsBySubreddit(Long id) {
        Subreddit subreddit = subredditRepository.findById(id)
                .orElseThrow(() -> new SubredditNotFoundException("No subreddit with id: " + id + " found"));
        return postRepository.findAllBySubredditOrderByCreatedDate(subreddit).stream()
                .map(PostResponse::new)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<PostResponse> getPostsByUserId(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new UsernameNotFoundException("User with id: " + id + " does not exist"));
        return postRepository.findByUser(user).stream()
                .map(PostResponse::new)
                .collect(Collectors.toList());
    }
}
