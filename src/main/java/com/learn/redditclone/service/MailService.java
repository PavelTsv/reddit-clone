package com.learn.redditclone.service;

import com.learn.redditclone.exceptions.FailedToSendEmailException;
import com.learn.redditclone.dto.NotificationEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailService {
    private final static String API_EMAIL = "reddit_clone@email.com";
    private final static String FAILED_TO_SEND_VER_EMAIL = "Failed when trying to send a verification email: ";
    private final JavaMailSender mailSender;
    private final MailContentBuilder mailContentBuilder;

    @Autowired
    public MailService(JavaMailSender mailSender, MailContentBuilder mailContentBuilder) {
        this.mailSender = mailSender;
        this.mailContentBuilder = mailContentBuilder;
    }

    @Async
    public void sendMail(NotificationEmail notificationEmail) {
        try {
            MimeMessagePreparator message = mapToMimeMessagePreparator(notificationEmail);
            mailSender.send(message);
        } catch (MailException e) {
            throw new FailedToSendEmailException(FAILED_TO_SEND_VER_EMAIL + e.getMessage());
        }
    }

    private MimeMessagePreparator mapToMimeMessagePreparator(NotificationEmail notificationEmail) {
        return mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(API_EMAIL);
            messageHelper.setTo(notificationEmail.getRecipient());
            messageHelper.setSubject(notificationEmail.getSubject());
            messageHelper.setText(mailContentBuilder.build(notificationEmail.getBody()));
        };
    }
}
