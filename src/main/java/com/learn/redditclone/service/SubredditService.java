package com.learn.redditclone.service;

import com.learn.redditclone.dto.SubredditCreationForm;
import com.learn.redditclone.exceptions.SubredditNotFoundException;
import com.learn.redditclone.model.Subreddit;
import com.learn.redditclone.repository.SubredditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SubredditService {
    private final SubredditRepository subredditRepository;

    @Autowired
    public SubredditService(SubredditRepository subredditRepository) {
        this.subredditRepository = subredditRepository;
    }

    @Transactional
    public Subreddit create(SubredditCreationForm subredditCreationForm) {
        Subreddit subreddit = new Subreddit(subredditCreationForm);
        subredditRepository.save(subreddit);
        return subreddit;
    }

    @Transactional(readOnly = true)
    public List<Subreddit> getAll() {
        return subredditRepository.findAll();
    }

    public Subreddit getSubreddit(Long id) {
        return subredditRepository.findById(id)
                .orElseThrow(() -> new SubredditNotFoundException("Subreddit by the id: " + id + " does not exist"));
    }
}
