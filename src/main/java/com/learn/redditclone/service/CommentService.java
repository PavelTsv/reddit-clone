package com.learn.redditclone.service;

import com.learn.redditclone.dto.CommentRequest;
import com.learn.redditclone.dto.CommentResponse;
import com.learn.redditclone.exceptions.PostNotFoundException;
import com.learn.redditclone.exceptions.UserNotFoundException;
import com.learn.redditclone.model.Comment;
import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.User;
import com.learn.redditclone.repository.CommentRepository;
import com.learn.redditclone.repository.PostRepository;
import com.learn.redditclone.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {
    private final UserRepository userRepository;
    private final PostRepository postRepository;
    private final CommentRepository commentRepository;
    private final AuthService authService;

    @Autowired
    public CommentService(UserRepository userRepository,
                          PostRepository postRepository,
                          CommentRepository commentRepository, AuthService authService) {
        this.userRepository = userRepository;
        this.postRepository = postRepository;
        this.commentRepository = commentRepository;
        this.authService = authService;
    }

    public Long save(CommentRequest commentRequest) {
        User user = authService.getCurrentUser()
                .orElseThrow(() -> new UserNotFoundException("User is not logged in"));
        Long postId = commentRequest.getPostId();
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post by id: " + postId + " not found"));
        Comment comment = new Comment(commentRequest, user, post);
        return commentRepository.save(comment).getId();
    }

    public List<CommentResponse> getAllCommentsForPost(Long postId) {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post with id: " + postId + " not found"));
        return commentRepository.findByPost(post).stream()
                .map(CommentResponse::new)
                .collect(Collectors.toList());
    }

    public List<CommentResponse> getAllCommentsForUser(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("User with id: " + userId + " not found"));
        return commentRepository.findByUser(user).stream()
                .map(CommentResponse::new)
                .collect(Collectors.toList());
    }
}
