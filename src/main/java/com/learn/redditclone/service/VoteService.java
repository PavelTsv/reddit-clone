package com.learn.redditclone.service;

import com.learn.redditclone.dto.VoteRequest;
import com.learn.redditclone.exceptions.PostNotFoundException;
import com.learn.redditclone.exceptions.UserNotFoundException;
import com.learn.redditclone.exceptions.VoteAlreadyExistsException;
import com.learn.redditclone.model.Post;
import com.learn.redditclone.model.User;
import com.learn.redditclone.model.Vote;
import com.learn.redditclone.repository.PostRepository;
import com.learn.redditclone.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class VoteService {
    PostRepository postRepository;
    VoteRepository voteRepository;
    AuthService authService;

    @Autowired
    public VoteService(PostRepository postRepository, VoteRepository voteRepository, AuthService authService) {
        this.postRepository = postRepository;
        this.voteRepository = voteRepository;
        this.authService = authService;
    }

    @Transactional
    public Long createVote(VoteRequest voteRequest) {
        User currentUser = authService.getCurrentUser()
                .orElseThrow(() -> new UserNotFoundException("No user is logged in"));
        Long postId = voteRequest.getPostId();
        Post postToUpvote = postRepository.findById(postId)
                .orElseThrow(() -> new PostNotFoundException("Post with id: " + postId + " not found"));
        Optional<Vote> vote = voteRepository.findByPostAndUser(postToUpvote, currentUser);
        int votePoints;
        if (vote.isEmpty()) {
            vote = Optional.of(new Vote(voteRequest, postToUpvote, currentUser));
            votePoints = voteRequest.getVoteType().getDirection();
        } else if (!vote.get().getVoteType().equals(voteRequest.getVoteType())) {
            vote.get().setVoteType(voteRequest.getVoteType());
            votePoints = 2 * voteRequest.getVoteType().getDirection();
        } else {
            throw new VoteAlreadyExistsException(
                    "User with id: " + currentUser.getId()
                            + " has already voted with:" + vote.get().getVoteType().name()
                            + " for post with id: " + postId);
        }
        postToUpvote.addCount(votePoints);
        postRepository.save(postToUpvote);
        voteRepository.save(vote.get());

        return vote.get().getId();
    }
}
