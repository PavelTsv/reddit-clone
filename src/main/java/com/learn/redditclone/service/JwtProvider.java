package com.learn.redditclone.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Base64;
import java.util.Date;

@Service
public class JwtProvider {
    private final byte[] secret = Base64.getDecoder().decode("I9sVOeG26h86a4R+U5THKgM4E4aibD9FJQKAr5oCJKw=");

    public String generateToken(Authentication authentication) {
        User principal = (User) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(principal.getUsername())
                .setIssuedAt(Date.from(Instant.now()))
                .signWith(Keys.hmacShaKeyFor(secret))
                .compact();
    }

    public void validateJwt(String jwt) {
        Jwts.parserBuilder()
                .setSigningKey(secret).build()
                .parseClaimsJws(jwt);
    }

    public String getUsernameFromJwt(String jwt) {
        return Jwts.parserBuilder().setSigningKey(secret).build()
                .parseClaimsJws(jwt).getBody().getSubject();
    }
}
