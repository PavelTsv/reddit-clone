package com.learn.redditclone.service;

import com.learn.redditclone.dto.AuthenticationResponse;
import com.learn.redditclone.dto.LoginRequest;
import com.learn.redditclone.dto.RegisterRequest;
import com.learn.redditclone.exceptions.InvalidVerificationTokenException;
import com.learn.redditclone.exceptions.UserAlreadyExistsException;
import com.learn.redditclone.exceptions.UserNotFoundException;
import com.learn.redditclone.model.User;
import com.learn.redditclone.model.VerificationToken;
import com.learn.redditclone.repository.UserRepository;
import com.learn.redditclone.repository.VerificationTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.Calendar;
import java.util.Optional;

@Service
public class AuthService {
    private static final String VERIFICATION_TOKEN_IS_EXPIRED = "Verification token is expired.";
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final VerificationTokenRepository verificationTokenRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;

    @Autowired
    public AuthService(PasswordEncoder passwordEncoder,
                       UserRepository userRepository,
                       VerificationTokenRepository verificationTokenRepository,
                       AuthenticationManager authenticationManager,
                       JwtProvider jwtProvider) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.verificationTokenRepository = verificationTokenRepository;
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @Transactional
    public User signup(@Valid final RegisterRequest registerRequest) {
        final var username = registerRequest.getUsername();
        final var email = registerRequest.getEmail();

        if (userRepository.existsByEmailOrUsername(email, username)) {
            throw new UserAlreadyExistsException();
        }

        return userRepository.save(new User(registerRequest, passwordEncoder));
    }

    @Transactional
    public void confirmRegistration(final String token) {
        final var verificationToken = verificationTokenRepository.findByToken(token)
                .orElseThrow(InvalidVerificationTokenException::new);
        final var username = verificationToken.getUser()
                .getUsername();
        final var user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotFoundException("No user with username: " + username + " found"));
        if (isVerificationTokenExpired(verificationToken)) {
            throw new InvalidVerificationTokenException(VERIFICATION_TOKEN_IS_EXPIRED);
        }
        user.setEnabled(true);
        userRepository.save(user);
    }

    private boolean isVerificationTokenExpired(final VerificationToken verificationToken) {
        final var calendar = Calendar.getInstance();

        return (verificationToken.getExpiryDate().getTime() - calendar.getTime().getTime()) <= 0;
    }

    public AuthenticationResponse login(@Valid final LoginRequest loginRequest) {
        final var authenticate = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authenticate);

        final var token = jwtProvider.generateToken(authenticate);

        return new AuthenticationResponse(token, loginRequest.getUsername());
    }

    @Transactional(readOnly = true)
    public Optional<User> getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Optional<User> user = Optional.empty();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            String currentUserName = authentication.getName();
            user = userRepository.findByUsername(currentUserName);
        }

        return user;
    }
}
