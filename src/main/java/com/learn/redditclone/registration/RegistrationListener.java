package com.learn.redditclone.registration;

import com.learn.redditclone.dto.NotificationEmail;
import com.learn.redditclone.model.VerificationToken;
import com.learn.redditclone.repository.VerificationTokenRepository;
import com.learn.redditclone.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;


@Component
public class RegistrationListener implements ApplicationListener<OnRegistrationCompleteEvent> {
    private final VerificationTokenRepository verificationTokenRepository;
    private final MailService mailService;
    @Value("${reddit-clone.app.verifyTokenExpirationMinutes}")
    private static int verificationTokenExpirationTimeInMinutes;

    @Autowired
    public RegistrationListener(final VerificationTokenRepository verificationTokenRepository,
                                final MailService mailService) {
        this.verificationTokenRepository = verificationTokenRepository;
        this.mailService = mailService;
    }

    @Override
    public void onApplicationEvent(final OnRegistrationCompleteEvent event) {
        final var user = event.getUser();
        final var verificationToken = new VerificationToken(user, verificationTokenExpirationTimeInMinutes);
        verificationTokenRepository.save(verificationToken);

        final var recipientAddress = user.getEmail();
        final var confirmationEmail = createDefaultVerificationEmail(
                recipientAddress,
                verificationToken.getToken(),
                event.getAppUrl());

        mailService.sendMail(confirmationEmail);
    }

    private NotificationEmail createDefaultVerificationEmail(final String userEmail,
                                                             final String token,
                                                             final String appUrl) {
        final var subject = "Confirm Registration";
        final var confirmationUrl = appUrl + "/api/auth/confirmRegistration/" + token;
        final var body = "Thank you for signing up to Reddit clone,"
                + "please click on the following url to activate your account :"
                + "\r\n" + confirmationUrl;

        return new NotificationEmail(subject, userEmail, body);
    }
}
