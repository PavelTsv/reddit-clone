package com.learn.redditclone.dto;

import java.util.Objects;

public class NotificationEmail {
    private final String subject;
    private final String recipient;
    private final String body;

    public NotificationEmail(String subject, String recipient, String body) {
        this.subject = subject;
        this.recipient = recipient;
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getBody() {
        return body;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationEmail that = (NotificationEmail) o;
        return Objects.equals(subject, that.subject)
                && Objects.equals(recipient, that.recipient)
                && Objects.equals(body, that.body);
    }

    @Override
    public int hashCode() {
        return Objects.hash(subject, recipient, body);
    }

    @Override
    public String toString() {
        return "NotificationEmail{" +
                "subject='" + subject + '\'' +
                ", recipient='" + recipient + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
