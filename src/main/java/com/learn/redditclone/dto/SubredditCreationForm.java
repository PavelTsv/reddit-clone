package com.learn.redditclone.dto;

public class SubredditCreationForm {
    private final String name;
    private final String desc;

    public SubredditCreationForm(String name, String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
