package com.learn.redditclone.dto;

import com.learn.redditclone.validators.PasswordMatches;
import com.learn.redditclone.validators.ValidEmail;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@PasswordMatches
public class RegisterRequest {
    @NotNull
    @NotEmpty
    private final String username;
    @NotNull
    @NotEmpty
    @ValidEmail
    private final String email;
    @NotNull
    @NotEmpty
    private final String password;
    private final String confirmationPassword;

    public RegisterRequest(String username, String email, String password, String confirmationPassword) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.confirmationPassword = confirmationPassword;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmationPassword() {
        return confirmationPassword;
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
