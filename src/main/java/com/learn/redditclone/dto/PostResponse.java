package com.learn.redditclone.dto;

import com.learn.redditclone.model.Post;

import java.time.Instant;

public class PostResponse {
    private final String name;
    private final String body;
    private final Long userId;
    private final Long subredditId;
    private final Integer upvotes;
    private final Instant createdDate;
    private final String url;

    public PostResponse(String name,
                        String body,
                        long userId,
                        Long subredditId,
                        Integer upvotes,
                        Instant createdDate,
                        String url) {
        this.name = name;
        this.body = body;
        this.userId = userId;
        this.subredditId = subredditId;
        this.upvotes = upvotes;
        this.createdDate = createdDate;
        this.url = url;
    }

    public PostResponse(Post post) {
        this.name = post.getPostName();
        this.body = post.getDescription();
        this.userId = post.getUser().getId();
        this.subredditId = post.getSubreddit().getId();
        this.upvotes = post.getVoteCount();
        this.createdDate = post.getCreatedDate();
        this.url = post.getUrl();
    }

    public String getName() {
        return name;
    }

    public String getBody() {
        return body;
    }

    public Long getUserId() {
        return userId;
    }

    public Long getSubredditId() {
        return subredditId;
    }

    public Integer getUpvotes() {
        return upvotes;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }
}
