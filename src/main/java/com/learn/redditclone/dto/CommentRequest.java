package com.learn.redditclone.dto;

public class CommentRequest {
    private final Long postId;
    private final String text;

    public CommentRequest(Long postId, String text) {
        this.postId = postId;
        this.text = text;
    }

    public Long getPostId() {
        return postId;
    }

    public String getText() {
        return text;
    }
}
