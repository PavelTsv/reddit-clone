package com.learn.redditclone.dto;

public class PostRequest {
    private final String subredditName;
    private final String name;
    private final String url;
    private final String desc;

    public PostRequest(String subredditName, String name, String url, String desc) {
        this.subredditName = subredditName;
        this.name = name;
        this.url = url;
        this.desc = desc;
    }

    public String getSubredditName() {
        return subredditName;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getDesc() {
        return desc;
    }
}
