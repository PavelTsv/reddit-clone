package com.learn.redditclone.dto;

import com.learn.redditclone.model.Comment;

import java.time.Instant;

public class CommentResponse {
    private final String text;
    private final Long postId;
    private final Instant postedOn;
    private final Long userId;

    public CommentResponse(String text, Long postId, Instant postedOn, Long userId) {
        this.text = text;
        this.postId = postId;
        this.postedOn = postedOn;
        this.userId = userId;
    }

    public CommentResponse(Comment comment) {
        this.text = comment.getText();
        this.postId = comment.getPost().getId();
        this.postedOn = comment.getDateOfCreation();
        this.userId = comment.getUser().getId();
    }

    public String getText() {
        return text;
    }

    public Long getPostId() {
        return postId;
    }

    public Instant getPostedOn() {
        return postedOn;
    }

    public Long getUserId() {
        return userId;
    }
}
