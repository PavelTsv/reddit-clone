package com.learn.redditclone.dto;

import com.learn.redditclone.model.VoteType;

public class VoteRequest {
    private final VoteType voteType;
    private final Long postId;

    public VoteRequest(VoteType voteType, Long postId) {
        this.voteType = voteType;
        this.postId = postId;
    }

    public VoteType getVoteType() {
        return voteType;
    }

    public Long getPostId() {
        return postId;
    }

    @Override
    public String toString() {
        return "VoteRequest{" +
                "voteType=" + voteType +
                ", postId=" + postId +
                '}';
    }
}
