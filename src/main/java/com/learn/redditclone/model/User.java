package com.learn.redditclone.model;

import com.learn.redditclone.dto.RegisterRequest;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.time.Instant;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String email;
    private Instant dateOfCreation;
    private boolean enabled;

    public User() {
    }

    public User(String username, String password, String email, Instant dateOfCreation, boolean enabled) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateOfCreation = dateOfCreation;
        this.enabled = enabled;
    }

    public User(RegisterRequest registerRequest, PasswordEncoder passwordEncoder) {
        this.username = registerRequest.getUsername();
        this.password = passwordEncoder.encode(registerRequest.getPassword());
        this.email = registerRequest.getEmail();
        this.dateOfCreation = Instant.now();
        this.enabled = false;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Instant getDateOfCreation() {
        return dateOfCreation;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", dateOfCreation=" + dateOfCreation +
                ", enabled=" + enabled +
                '}';
    }
}
