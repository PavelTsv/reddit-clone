package com.learn.redditclone.model;


import com.learn.redditclone.dto.CommentRequest;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;

@Entity
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String text;
    @ManyToOne
    @JoinColumn(name = "postId")
    private Post post;
    @Column(nullable = false, updatable = false)
    private Instant dateOfCreation;
    @ManyToOne
    @JoinColumn(name = "userId", nullable = false, updatable = false)
    private User user;

    public Comment() {
    }

    public Comment(String text, Post post, Instant dateOfCreation, User user) {
        this.text = text;
        this.post = post;
        this.dateOfCreation = dateOfCreation;
        this.user = user;
    }

    public Comment(CommentRequest commentRequest, User user, Post post) {
        this.dateOfCreation = Instant.now();
        this.user = user;
        this.post = post;
        this.text = commentRequest.getText();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Instant getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Instant dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id.equals(comment.id) && Objects.equals(text, comment.text)
                && Objects.equals(post, comment.post)
                && dateOfCreation.equals(comment.dateOfCreation)
                && Objects.equals(user, comment.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, post, dateOfCreation, user);
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", post=" + post +
                ", dateOfCreation=" + dateOfCreation +
                ", user=" + user +
                '}';
    }
}
