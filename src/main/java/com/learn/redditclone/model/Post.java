package com.learn.redditclone.model;

import com.learn.redditclone.dto.PostRequest;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.Instant;
import java.util.Objects;


@Entity
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String postName;
    private String url;
    private String description;
    private Integer voteCount;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;
    @Column(nullable = false, updatable = false)
    private Instant createdDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "subredditId")
    private Subreddit subreddit;

    public Post() {
    }

    public Post(String postName, @Nullable String url, String description, Integer voteCount, User user, Instant createdDate, Subreddit subreddit) {
        this.postName = postName;
        this.url = url;
        this.description = description;
        this.voteCount = voteCount;
        this.user = user;
        this.createdDate = createdDate;
        this.subreddit = subreddit;
    }

    public Post(PostRequest postRequest, Subreddit subreddit, User user) {
        this.postName = postRequest.getName();
        this.url = "r/" + postRequest.getSubredditName();
        this.description = postRequest.getDesc();
        this.voteCount = 0;
        this.user = user;
        this.createdDate = Instant.now();
        this.subreddit = subreddit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public Subreddit getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(Subreddit subreddit) {
        this.subreddit = subreddit;
    }

    public void addCount(Integer vote) {
        this.voteCount += vote;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return Objects.equals(id, post.id)
                && Objects.equals(postName, post.postName)
                && Objects.equals(url, post.url)
                && Objects.equals(description, post.description)
                && Objects.equals(voteCount, post.voteCount)
                && Objects.equals(user, post.user)
                && Objects.equals(createdDate, post.createdDate)
                && Objects.equals(subreddit, post.subreddit);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, postName, url, description, voteCount, user, createdDate, subreddit);
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", postName='" + postName + '\'' +
                ", url='" + url + '\'' +
                ", description='" + description + '\'' +
                ", voteCount=" + voteCount +
                ", user=" + user +
                ", createdDate=" + createdDate +
                ", subreddit=" + subreddit +
                '}';
    }
}
