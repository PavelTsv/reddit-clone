package com.learn.redditclone.model;


import com.learn.redditclone.dto.SubredditCreationForm;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Subreddit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Post> posts;
    private Date createdDate;
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    public Subreddit() {
    }

    public Subreddit(String name, String description, List<Post> posts, Date createdDate) {
        this.name = name;
        this.description = description;
        this.posts = posts;
        this.createdDate = createdDate;
    }

    public Subreddit(SubredditCreationForm subredditCreationForm) {
        this.name = subredditCreationForm.getName();
        this.description = subredditCreationForm.getDesc();
        this.posts = new ArrayList<>();
        this.createdDate = Date.from(Instant.now());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Subreddit{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", posts=" + posts +
                ", createdDate=" + createdDate +
                ", user=" + user +
                '}';
    }
}
