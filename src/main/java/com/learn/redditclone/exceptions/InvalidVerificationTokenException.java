package com.learn.redditclone.exceptions;

public class InvalidVerificationTokenException extends RuntimeException {
    private static final String DEFAULT_MESSAGE = "The given verification code is invalid";

    public InvalidVerificationTokenException() {
        super(DEFAULT_MESSAGE);
    }

    public InvalidVerificationTokenException(String message) {
        super(message);
    }
}
