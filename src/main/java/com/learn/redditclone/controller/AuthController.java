package com.learn.redditclone.controller;

import com.learn.redditclone.dto.AuthenticationResponse;
import com.learn.redditclone.dto.LoginRequest;
import com.learn.redditclone.dto.RegisterRequest;
import com.learn.redditclone.service.AuthService;
import com.learn.redditclone.registration.OnRegistrationCompleteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private static final String ACCOUNT_ACTIVATED_SUCCESSFULLY = "Account Activated Successfully";
    private final AuthService authService;
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public AuthController(final AuthService authService,
                          final ApplicationEventPublisher eventPublisher) {
        this.authService = authService;
        this.eventPublisher = eventPublisher;
    }

    @PostMapping("/signup")
    public ResponseEntity<Long> signup(@Valid @RequestBody final RegisterRequest registerRequest,
                                       final HttpServletRequest request) {
        final var user = authService.signup(registerRequest);
        eventPublisher.publishEvent(new OnRegistrationCompleteEvent(user, getAppUrl(request)));

        return new ResponseEntity<>(user.getId(), HttpStatus.CREATED);
    }

    private String getAppUrl(final HttpServletRequest request) {
        return "http://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
    }

    @GetMapping("/confirmRegistration/{token}")
    public ResponseEntity<String> confirmRegistration(@PathVariable String token) {
        authService.confirmRegistration(token);
        return new ResponseEntity<>(ACCOUNT_ACTIVATED_SUCCESSFULLY, HttpStatus.OK);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginRequest loginRequest) {
        return new ResponseEntity<>(authService.login(loginRequest), HttpStatus.OK);
    }
}
