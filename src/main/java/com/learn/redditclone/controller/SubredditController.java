package com.learn.redditclone.controller;

import com.learn.redditclone.dto.SubredditCreationForm;
import com.learn.redditclone.model.Subreddit;
import com.learn.redditclone.service.SubredditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/r")
public class SubredditController {
    private final SubredditService subredditService;

    @Autowired
    public SubredditController(SubredditService subredditService) {
        this.subredditService = subredditService;
    }

    @PostMapping
    public ResponseEntity<Subreddit> createSubreddit(@RequestBody SubredditCreationForm subredditCreationForm) {
        Subreddit subreddit = subredditService.create(subredditCreationForm);
        return ResponseEntity.status(HttpStatus.CREATED).body(subreddit);
    }

    @GetMapping
    public ResponseEntity<List<Subreddit>> getAllSubreddits() {
        List<Subreddit> subreddits = subredditService.getAll();
        return ResponseEntity.status(HttpStatus.OK)
                .body(subreddits);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subreddit> getSubreddit(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(subredditService.getSubreddit(id));
    }
}
