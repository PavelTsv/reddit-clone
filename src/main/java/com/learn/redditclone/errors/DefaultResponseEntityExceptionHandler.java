package com.learn.redditclone.errors;

import com.learn.redditclone.exceptions.InvalidVerificationTokenException;
import com.learn.redditclone.exceptions.UserAlreadyExistsException;
import com.learn.redditclone.exceptions.UserNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class DefaultResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {
            UserAlreadyExistsException.class,
            UserNotFoundException.class,
            InvalidVerificationTokenException.class})
    protected ResponseEntity<Object> handleGlobalException(RuntimeException exception, WebRequest request) {
        return handleExceptionInternal
                (exception,
                        exception.getMessage(),
                        new HttpHeaders(),
                        HttpStatus.BAD_REQUEST,
                        request);
    }


}
